package com.aal.luis.beertab;

/**
 * Created by Luis Valenzuela on 3/17/2016.
 */
public class Drink {

    private int drinkID = 0;
    private String drinkName = null;
    private Double drinkCost = .0;
    private String drinkImageId = null;
    private int drinkQty = 0;

    public Drink(){}

    public int getDrinkID() {
        return drinkID;
    }

    public void setDrinkID(int inputDrinkID) {
        drinkID = inputDrinkID;
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String inputDrinkName) {
        drinkName = inputDrinkName;
    }

    public double getDrinkCost() {
        return drinkCost;
    }

    public void setDrinkCost(double inputDrinkCost) {
        drinkCost = inputDrinkCost;
    }

    public String getDrinkImageId() {
        return drinkImageId;
    }

    public void setDrinkImageId(String inputDrinkImageId) {
        drinkImageId = inputDrinkImageId;
    }

    public int getDrinkQty() {
        return drinkQty;
    }

    public void setDrinkQty(int inputDrinkQty) {
        drinkQty =inputDrinkQty ;
    }

}
