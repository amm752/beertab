package com.aal.luis.beertab;

import android.app.Application;
import java.util.Hashtable;


/**
 * Created by Luis Valenzuela on 3/20/2016.
 */
public class AddDrinkToOrder {

    private Hashtable drinksBag = new Hashtable();
    private int length = 0;

    public AddDrinkToOrder() {
        drinksBag = new Hashtable();
    }

    public void addDrink(int drinkID, int drinkQty) {
        drinksBag.put(drinkID,drinkQty);

        length++;
    }

    public String getQtyofDrink(int id) {
        return drinksBag.get(id).toString();
    }

    public int getDrinkArrayLength() {
        return length;
    }
}
