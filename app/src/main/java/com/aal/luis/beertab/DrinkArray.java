package com.aal.luis.beertab;

import java.util.ArrayList;

/**
 * Created by Luis Valenzuela on 3/20/2016.
 */
public class DrinkArray {

    private ArrayList<Drink> drinkArrayList;
    private int length = 0;

    public DrinkArray() {
        drinkArrayList = new ArrayList<Drink>();
    }

    public void addDrink(int drinkID, String drinkName, double drinkCost, String drinkImageId,int drinkQty) {
        Drink drink = new Drink();

        drink.setDrinkID(drinkID);
        drink.setDrinkName(drinkName);
        drink.setDrinkCost(drinkCost);
        drink.setDrinkQty(drinkQty);
        drink.setDrinkImageId(drinkImageId);

        drinkArrayList.add(drink);
        length++;
    }

    public void add(Drink drink) {
        drinkArrayList.add(drink);
    }

    public Drink getDrinkAtIndex(int index) {
        return drinkArrayList.get(index);
    }

    public ArrayList getDrinkArray (){
        return drinkArrayList;
    }

    public int getDrinkArrayLength() {
        return length;
    }
}
