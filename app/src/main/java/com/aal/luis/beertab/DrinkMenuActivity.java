package com.aal.luis.beertab;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Spinner;


import org.w3c.dom.Text;

import java.net.URL;
import java.util.ArrayList;

public class DrinkMenuActivity extends BaseActivity {
    Global global = Global.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drink_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Drink Menu");

        DrinkArray testArray = initDatabase();

        final ListView listView = (ListView) findViewById(R.id.drinkMenu);
        listView.setAdapter(new DrinkBaseAdapter(this, testArray));
        setDrawerContents();

        checkExtras();

        FloatingActionButton reviewOrderButton = (FloatingActionButton) findViewById(R.id.reviewOrderButton);

        reviewOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ReviewOrderActivity.class);
                startActivity(intent);
            }
        });


    }

    public void checkExtras() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        String personName = extras.getString("EXTRA_PERSON_NAME");
        String tableNumber = extras.getString("EXTRA_TABLE_NUMBER");

        Toast.makeText(this, "Person Name: " + personName + "\n" + "Table Number: " + tableNumber, Toast.LENGTH_LONG).show();
    }

    public DrinkArray initDatabase() {
        DatabaseHelper dbHelper = new DatabaseHelper(this, null, null, 1);
        DrinkArray drinkArray = new DrinkArray();




        /**************************************************************/
        int i = 0;
        while (i < 10) {
            drinkArray.addDrink(i, "Test Drink: " + i, 2.00, null,0);
            //global.addDrinkToOrder(drinkArray.getDrinkAtIndex(i));
            //System.out.println("Global: " + global.getOrderContents().getDrinkAtIndex(i).getDrinkName());
            i++;
        }

        return drinkArray;
    }

}
