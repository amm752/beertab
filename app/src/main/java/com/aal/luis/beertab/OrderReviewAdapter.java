package com.aal.luis.beertab;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.Intent;



public class OrderReviewAdapter extends BaseAdapter{
    private LayoutInflater inflater;
    private static DrinkArray orders;

    public OrderReviewAdapter(Context context, DrinkArray orderList) {
        orders = orderList;
        inflater = LayoutInflater.from(context);


    }

    public int getCount() {
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final MultiViewHolder viewHolder;
        //RecyclerView.ViewHolder holder;
        //View tempView = new View
        System.out.println("In OrderAdapter");

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.multi_row_orders, null);
            viewHolder = new MultiViewHolder();
            viewHolder.drinkImageView = (ImageView) convertView.findViewById(R.id.drinkImageOrder);
            viewHolder.drinkNameView = (TextView) convertView.findViewById(R.id.drinkNameOrder);
            viewHolder.drinkCostView = (TextView) convertView.findViewById(R.id.drinkCostOrder);
            viewHolder.updateOrderButton = (Button) convertView.findViewById(R.id.updateOrder);
            viewHolder.removeOrderButton = (Button) convertView.findViewById(R.id.removeOrder);
            viewHolder.drinkQty = (Spinner) convertView.findViewById(R.id.drinkQtyOrder);
            viewHolder.displayQtyOrder = (TextView) convertView.findViewById(R.id.displayQtytOrder);

            convertView.setTag(viewHolder);

        }
        else {
            viewHolder = (MultiViewHolder) convertView.getTag();

        }
        viewHolder.drinkImageView.setImageResource(R.mipmap.pep);
        viewHolder.drinkNameView.setText(orders.getDrinkAtIndex(position).getDrinkName());
        viewHolder.drinkCostView.setText(Double.toString(orders.getDrinkAtIndex(position).getDrinkCost()));
        viewHolder.displayQtyOrder.setText(orders.getDrinkAtIndex(position).toString());
        viewHolder.updateOrderButton.setVisibility(viewHolder.updateOrderButton.VISIBLE);
        viewHolder.removeOrderButton.setVisibility(viewHolder.removeOrderButton.VISIBLE);
        viewHolder.drinkQty.setVisibility(viewHolder.drinkQty.VISIBLE);
        //viewHolder.displayQtytOrder.setText(orders.getDrinkAtIndex(0).getDrinkQty());

        return convertView;
    }

    static class MultiViewHolder {
        ImageView drinkImageView;
        TextView drinkNameView;
        TextView drinkCostView;
        TextView displayQtyOrder;
        Button updateOrderButton;
        Button removeOrderButton;
        Spinner drinkQty;
    }

}
