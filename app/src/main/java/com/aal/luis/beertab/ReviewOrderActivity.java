package com.aal.luis.beertab;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class ReviewOrderActivity extends BaseActivity {
    Global global = Global.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toast.makeText(this, "Started Review", Toast.LENGTH_LONG).show();
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_review_order);
        setContentView(R.layout.activity_review_order);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOrder);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //toolbar.setTitle("Cart");
        //toolbar.setTitle("Order Menu");
        //setSupportActionBar(toolbar);

        //System.out.println(DrinkBaseAdapter.drinkBag.getDrinkAtIndex(0).getDrinkQty());

        //final ListView listView = (ListView) findViewById(R.id.drinkOrders);
        //final ListView listView = (ListView) findViewById(R.id.drinkMenu);
        //DrinkArray temp2 = new DrinkArray();
        //temp2.addDrink(1, "Hello", 0, null, 4);

        //DrinkArray temp = new DrinkArray();
        //int i = 0;
        /*while (i < global.getOrderContents().getDrinkArrayLength()) {
            temp.add(global.getOrderContents().getDrinkAtIndex(i));
            System.out.println("Drink Name: " + global.getOrderContents().getDrinkAtIndex(i).getDrinkName() +
                                "\nDrink Quantity: " + global.getOrderContents().getDrinkAtIndex(i).getDrinkQty());
            i++;
        }*/

        //listView.setAdapter(new OrderReviewAdapter(this, temp2));

        //final ListView listView = (ListView) findViewById(R.id.drinkMenu);
//        listView.setAdapter(new DrinkBaseAdapter(this, global.getOrderContents()));




        //ListView menu = (ListView) findViewById(R.id.drinkOrders);

        FloatingActionButton reviewOrderButton = (FloatingActionButton) findViewById(R.id.orderProgressButton);
        reviewOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), OrderProgress.class);
                startActivity(intent);
            }
        });

        setContentView(R.layout.activity_review_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarOrder);
        toolbar.setTitle("Cart");


        final ListView listView = (ListView) findViewById(R.id.drinkOrders);

        System.out.println("Global order: " + global.getOrderContents().getDrinkArrayLength());

        listView.setAdapter(new DrinkOrderAdapter(this, global.getOrderContents()));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_review_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
