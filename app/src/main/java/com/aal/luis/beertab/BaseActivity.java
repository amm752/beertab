package com.aal.luis.beertab;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;

import java.util.List;
/* This class is used to set up the side menu for the activities. Itself extends activity and
*  so the rest of the activities can extend this BaseActivity class and inherit all of the properties
*  from the Activity class as well as th overridden methods defined in this class. To have the menu
*  available just have your activity extend this class*/
public class BaseActivity extends Activity {
    private Global global;

    private String[] drawerTitles = {"DashBoard", "Alcholic-Drinks", "non-Alcholic-Drinks","Food"};
    protected DrawerLayout fullLayout;
    protected FrameLayout frameLayout;
    protected ListView drawerContents;
    //protected Toolbar toolbar;
    private View itemView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        global = Global.getInstance();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        fullLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        frameLayout = (FrameLayout) fullLayout.findViewById(R.id.content_frame);

        getLayoutInflater().inflate(layoutResID, frameLayout, true);
        super.setContentView(fullLayout);
    }

    public void setDrawerContents() {
        drawerContents = (ListView) findViewById(R.id.drawerListView);
        drawerContents.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, global.getDrawerTitles()));
    }
}
