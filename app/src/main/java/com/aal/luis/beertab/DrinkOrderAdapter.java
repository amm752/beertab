package com.aal.luis.beertab;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Luis Valenzuela on 4/19/2016.
 */
public class DrinkOrderAdapter extends BaseAdapter {
        private static DrinkArray order;
        private LayoutInflater inflater;

    public DrinkOrderAdapter(Context context, DrinkArray drinkList) {
        order = drinkList;
        inflater = LayoutInflater.from(context);
    }

    public int getCount() { return order.getDrinkArrayLength();}

    public Drink getItem(int index) { return order.getDrinkAtIndex(index);}

    public long getItemId(int position) { return position; }

    public View getView (final int position, View convertView, ViewGroup parent) {
        final MultiViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.multi_row_review_order, null);
            viewHolder = new MultiViewHolder();

            viewHolder.drinkImageView = (ImageView) convertView.findViewById(R.id.drinkOrderImage);
            viewHolder.drinkTextView = (TextView) convertView.findViewById(R.id.drinkOrderQuantityText);
            viewHolder.drinkQuantitySpinner = (Spinner) convertView.findViewById(R.id.drinkOrderSpinner);
            viewHolder.drinkNameView = (TextView) convertView.findViewById(R.id.drinkNameOrder);
            viewHolder.drinkCostView = (TextView) convertView.findViewById(R.id.drinkCostOrder);
            viewHolder.drinkRemoveButton = (Button) convertView.findViewById(R.id.removeOrder);
            viewHolder.drinkUpdateOrderButton = (Button) convertView.findViewById(R.id.updateOrder);

            convertView.setTag(viewHolder);

        }
        else {
            viewHolder = (MultiViewHolder) convertView.getTag();
        }

        viewHolder.drinkImageView.setImageResource(R.mipmap.pep);
        viewHolder.drinkQuantitySpinner.setSelection(order.getDrinkAtIndex(position).getDrinkQty());
        viewHolder.drinkNameView.setText(order.getDrinkAtIndex(position).getDrinkName());
        viewHolder.drinkCostView.setText(String.format(Double.toString(order.getDrinkAtIndex(position).getDrinkCost() *
                order.getDrinkAtIndex(position).getDrinkQty())));
        return convertView;
    }

    static class MultiViewHolder {
        ImageView drinkImageView;
        TextView drinkTextView;
        Spinner drinkQuantitySpinner;
        TextView drinkNameView;
        TextView drinkCostView;
        Button drinkRemoveButton;
        Button drinkUpdateOrderButton;
    }
}
