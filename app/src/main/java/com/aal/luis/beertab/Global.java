package com.aal.luis.beertab;

import android.app.Application;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Luis Valenzuela on 4/17/2016.
 */

/* This function is meant to store any global variables that we may need. If you want to add a variable
*  just declare it here and make getter and setter methods*/
public class Global {
    private static Global instance;

    private DrinkArray drinkOrder = new DrinkArray();
    private String[] drawerTitles = {"DashBoard", "Alcholic-Drinks", "non-Alcholic-Drinks","Food"};
/*    private String[] drawerTitles = {"Home", "Menu", "Cart"};
    private DrawerLayout drawerLayout;
    private ListView drawerContents;*/



    private Global(){}

    public static synchronized Global getInstance() {
        if (instance == null) {
            instance = new Global();
        }
        return instance;
   }

    public String[] getDrawerTitles() {
        return drawerTitles;
    }



    public DrinkArray getOrderContents() {
        return drinkOrder;
    }

    public void addDrinkToOrder(Drink drink) {
        this.drinkOrder.add(drink);
    }

    public void setEntireOrder(DrinkArray drinkArray) {
        drinkOrder = drinkArray;
    }

}
